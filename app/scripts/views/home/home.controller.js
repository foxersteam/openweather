(function () {
    'use strict';

    angular
        .module('openWeather')
        .controller('homeController', homeController);

    homeController.$inject = [
        'dataContext',
        'dataStore'
    ];


    function homeController(
        dataContext,
        dataStore
    ) {

        var vm = this;
        vm.cites = [];
        vm.widgets = [];
        activate();

        ////////////////

        function activate() {
            vm.cites = dataStore.list();
            var index = 0,
                iMax = vm.cites.length;

            for (; index < iMax; index++) {
                //getWeather
                dataContext.weatherService.getById(vm.cites[index].id).then(
                    function success(data) {
                        vm.widgets.push(data);
                    },
                    function error(err) {
                        console.error(JSON.stringify(err));
                    }
                );



            }

        }
    }
})();