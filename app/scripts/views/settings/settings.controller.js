(function () {
    'use strict';

    angular
        .module('openWeather')
        .controller('settingsController', settingsController);

    settingsController.$inject = [
        'dataContext',
        'dataStore',
        'appConfig'
    ];

    function settingsController(
        dataContext,
        dataStore,
        appConfig
    ) {
        var vm = this;
        vm.searchResult = [];
        vm.searchTerm = '';
        vm.iconUrl = appConfig.iconUrl
        vm.widgets = [];
        vm.loading = false;

        activate();
        ////////////////

        function activate() {
            vm.widgets = dataStore.list();
        }

        //////////////// UI ////////////////

        vm.searchCity = function (searchTerm) {
            vm.loading = true;
            dataContext.cityService.search(searchTerm)
                .then(listCities_success, listCities_error);
        }

        vm.addCity = function (item) {
            vm.widgets.push(item);
            dataStore.save(vm.widgets);
            clearSearch();
        }

        vm.removeCity = function (index) {
            vm.widgets.splice(index, 1);
            dataStore.save(vm.widgets);
        }

        //////////////// CALLBACK ////////////////

        function listCities_success(data) {
            vm.loading = false;
            vm.searchResult = data.list;
        }

        function listCities_error(err) {
            vm.loading = false;
            console.log('error');
        }

        function clearSearch() {
            vm.searchResult = null;
            vm.searchTerm = '';
        }
    }
})();