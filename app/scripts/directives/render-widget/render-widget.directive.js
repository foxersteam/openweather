(function () {
    'use strict';

    angular
        .module('openWeather')
        .directive('renderWidget', renderWidget);

    renderWidget.$inject = ['appConfig'];

    function renderWidget(appConfig) {
        // Usage:
        //
        // Creates:
        //
        var directive = {
            bindToController: true,
            controller: renderWidgetDirectiveController,
            controllerAs: 'vm',
            templateUrl: 'scripts/directives/render-widget/render-widget.html',
            link: link,
            restrict: 'A',
            scope: {
                item: '=item'
            }
        };
        return directive;

        function link(scope, element, attrs) {
            
        }
    }
    /* @ngInject */
    function renderWidgetDirectiveController(appConfig) {
        var vm = this;
        vm.currentForecast = vm.item.list[0];
        vm.iconUrl = appConfig.iconUrl;

        vm.getDate = function(dt_txt){
            return new Date(dt_txt);
        }


        vm.toggle = function(){
            if(vm.item.visible === null){
                vm.item.visible = false;
            }

            vm.item.visible = !vm.item.visible;
        }
    }

    
})();