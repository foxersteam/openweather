(function () {
    'use strict';

    angular
        .module('openWeather')
        .service('dataContext', dataContext);

    dataContext.$inject = [
        'cityService',
        'weatherService'
    ];

    function dataContext(
        cityService,
        weatherService
    ) {
        var self = {
            cityService : cityService,
            weatherService : weatherService
        };

        return self;
    }
})();