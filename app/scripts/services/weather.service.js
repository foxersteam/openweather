(function () {
    'use strict';

    angular
        .module('openWeather')
        .service('weatherService', weatherService);

    weatherService.$inject = [
        '$http',
        '$q',
        'appConfig'
    ];

    function weatherService(
        $http,
        $q,
        appConfig
    ) {

        function getById(cityId) {
            var deferred = $q.defer();
            var url = appConfig.url.forecast.replace(':apiKey', appConfig.apiKey);
            url = url.replace(':cityId', cityId);

            $http({
                method: 'GET',
                url: url,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(
                function successCallback(response) {
                    deferred.resolve(response.data);
                },
                function errorCallback(error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        return {
            getById: getById
        }
    }
})();