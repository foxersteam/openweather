(function () {
    'use strict';

    angular
        .module('openWeather')
        .service('cityService', cityService);

    cityService.$inject = [
        '$http',
        '$q',
        'appConfig'
    ];

    function cityService(
        $http,
        $q,
        appConfig
    ) {

        var self = {};

        self.search = function (cityName) {
            var deferred = $q.defer();
            var url = appConfig.url.searchCity.replace(':apiKey', appConfig.apiKey);
            url = url.replace(':cityName', cityName);

            $http({
                method: 'GET',
                url: url,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(
                function successCallback(response) {
                    deferred.resolve(response.data);
                },
                function errorCallback(error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        };

        return self;
    }
})();