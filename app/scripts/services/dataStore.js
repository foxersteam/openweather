(function () {
    'use strict';

    angular
        .module('openWeather')
        .service('dataStore', dataStore);

    dataStore.$inject = ['localStorageService'];



    function dataStore(localStorageService) {

        var key = 'CITIES';

        
        function list() {
            var cities = localStorageService.get(key);
            if (cities == undefined) {
                cities = [];
                save(cities);
            }
            return cities;
        }

        function save(data) {
            localStorageService.set(key, data);
        }

        return {
            list: list,
            save: save
        };
    }
})();