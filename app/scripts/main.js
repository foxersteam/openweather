(function () {
    'use strict';

    angular.module('openWeather', [
        'ngRoute',
        'LocalStorageModule'
    ])
        .config(function ($locationProvider) {
            $locationProvider.html5Mode({
                enabled: true,
                requireBase: true,
                rewriteLinks: true
            });
        })
        .config(function (localStorageServiceProvider) {
            localStorageServiceProvider
                .setPrefix('OW');
        })
        .constant('appConfig', {
            apiKey: '47cac7057bb4c1f7bb7dbb67923e4c30',
            url: {
                searchCity: 'http://api.openweathermap.org/data/2.5/find?q=:cityName&type=like&appid=:apiKey&units=metric',
                weather: 'http://api.openweathermap.org/data/2.5/weather?id=:cityId&APPID=:apiKey&units=metric',
                forecast: 'http://api.openweathermap.org/data/2.5/forecast/city?id=:cityId&APPID=:apiKey&units=metric'
            },
            iconUrl: 'http://openweathermap.org/img/w/'
        })
        .config(function ($routeProvider) {
            $routeProvider
                .when('/', {
                    templateUrl: 'scripts/views/home/home.html',
                    controller: 'homeController',
                    controllerAs: 'vm'
                })
                .when('/settings', {
                    templateUrl: 'scripts/views/settings/settings.html',
                    controller: 'settingsController',
                    controllerAs: 'vm'
                })
                .otherwise({
                    redirectTo: '/'
                });
        });
})();


