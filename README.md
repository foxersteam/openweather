# README #

Weather Dashboard
Projet de démo utilisant l'api OpenWeatherMap.

Développé par Guillaume Duprez - [gduprez@gmail.com](mailto:gduprez@gmail.com)

## Installation du poste de développement ##

### Node Js - NPM ###
Nécessite l'installation de NodeJs version LTS (4.x)

### Import des sources ###
Placer vous dans le repertoire souhaité puis executer la commande 
```
#!bash
git clone git@bitbucket.org:foxersteam/openweather.git
```

### Dépendances serveur ###
En ligne de commande, dans le repertoire de l'application, executer la commande 
```
#!bash
npm install -g gulp
npm install
```

### Dépendances client ###
Installer Bower en mode global:
```
#!bash
npm install bower -g
```
Dans le repertoire de l'application, executer la commande:
```
#!bash
bower install
```

### Lancer le site en local ###
```
#!bash
gulp serve
```
la commande crée un serveur HTTP et lance le site en localhost:9000.
Les modifications serveur et clients provoque le rechargement de la page via LiveReload.


## Instructions de déploiement ##

```
#!bash
gulp build
```
Copier le contenu du repertoire dist sur le serveur.